#-*- coding: utf-8 -*-

import os, sys

from django.core.management.base import BaseCommand, CommandError
from django.utils import simplejson as json

class Commandline(BaseCommand):
    BaseError = CommandError
    
    def log(self, msg):
        self.stdout.write(msg)
    
    def shell(self, *cmd):
        stmt = ' '.join(cmd)
        
        os.system(stmt)
    
    def load_json(self, path, **kwargs):
        return json.load(open(path), **kwargs)

