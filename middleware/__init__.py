#-*- coding: utf-8 -*-

from django2use.shortcuts import *

from new import instancemethod

from django.core.handlers.wsgi import WSGIRequest
from django.utils.cache import patch_vary_headers

def request_redirect(request, target):
    return HttpResponseRedirect(target)

def request_render_tpl(request, tpl, **cnt):
    cnt['curr_request'] = request
    cnt['curr_user']    = request.user
    
    if hasattr(request, 'theme_layout'):
        cnt['curr_layout']  = request.theme_layout
    
    try:
        cnt['curr_profile'] = request.user.get_profile()
    except:
        cnt['curr_profile'] = None
    
    return render_to_response(tpl, RequestContext(request, cnt))

def request_render_json(request, data, **cnt):
    resp = HttpResponse(json.dumps(data))
    
    resp['Content-Type'] = 'application/json'
    
    return resp

class Augment:
    def process_request(self, request):
        setattr(request, 'redirect', instancemethod(request_redirect, request, WSGIRequest))
        
        setattr(request, 'render_json', instancemethod(request_render_json, request, WSGIRequest))
        
        setattr(request, 'render_tpl', instancemethod(request_render_tpl, request, WSGIRequest))
        
        setattr(request, 'host', request.META["HTTP_HOST"])
        
        for port in [80, 8000]:
            flg = ':%d' % port
            
            if request.host[-len(flg):] == flg:
                request.host = request.host[:-len(flg)]

def Extend(request):
    return {
        'page':  {
            'title':  "Acceuil",
            'author': "Tayaa Med Amine",
        },
    }

