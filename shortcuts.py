#-*- coding: utf-8 -*-

from __future__ import absolute_import

import inspect
from new import instancemethod

import os, sys, logging, shlex, subprocess
import operator, random, uuid, base64
import re, urlparse, xmlrpclib, requests
import yaml, jinja2

from datetime import date, time, datetime

from markdown import markdown

from django import dispatch, forms
from django.conf import urls as dj_urls
from django.core.exceptions import MiddlewareNotUsed
from django.core.handlers.wsgi import WSGIRequest
from django.core.management import call_command
from django.db import models
from django.db.models import Q
from django.forms.formsets import formset_factory
from django.http import HttpResponse, HttpResponseRedirect, Http404

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType

from django.shortcuts import render_to_response
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.utils import simplejson as json, timezone as TZ
from django.utils.encoding import python_2_unicode_compatible

from django_extensions.db.fields import UUIDField

from celery import task, shared_task

from django2use.shell import Commandline

from django2use.constants import *
from django2use.utils import *
from django2use.utils.POO import *

