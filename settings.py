#-*- coding: utf-8 -*-

import os, urlparse#, random

there = lambda *x: os.path.join(os.path.dirname(os.path.dirname(__file__)), *x)
here = lambda *x: there('files', *x)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

DEFAULT_NSAP_STRATEGY = None

from datetime import datetime

STARTED_ON = datetime.now()

