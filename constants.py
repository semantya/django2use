#-*- coding: utf-8 -*-

import yaml
from django.utils import simplejson as json

REST_RENDERERs = dict(
    json = dict(
        mimes = ['application/json'],
        dump  = lambda x: json.dumps(x),
        load  = lambda x: json.loads(x),
    ),
    yaml = dict(
        mimes = ['text/yaml'],
        dump  = lambda x: yaml.dumps(x, pretty=True),
        load  = lambda x: yaml.loads(x),
    ),
)

STD_SCALARs = [
    bool, int, long, float,
]
STD_TEXTs = [
    str, unicode,
]
STD_LISTs = [
    set, frozenset, list,
]
STD_HASHs = [dict]

STD_TYPEs = STD_SCALARs + STD_TEXTs + STD_LISTs + STD_HASHs

