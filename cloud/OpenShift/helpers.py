#-*- coding: utf-8 -*-

import os, urlparse

def parse_env_db():
    def openshift_mysql():
        url = urlparse.urlparse(os.environ.get('OPENSHIFT_MYSQL_DB_URL'))
        
        return {
            'ENGINE' : 'django.db.backends.mysql',
            'NAME': os.environ['OPENSHIFT_APP_NAME'],
            'USER': url.username,
            'PASSWORD': url.password,
            'HOST': url.hostname,
            'PORT': int(url.port or '3306'),
        }
    
    #*************************************************************************
    
    def openshift_pgsql():
        url = urlparse.urlparse(os.environ.get('OPENSHIFT_POSTGRESQL_DB_URL'))
        
        return {
            'ENGINE' : 'django.db.backends.postgresql_psycopg2',
            'NAME': os.environ['OPENSHIFT_APP_NAME'],
            'USER': url.username,
            'PASSWORD': url.password,
            'HOST': url.hostname,
            'PORT': int(url.port or '3306'),
        }
    
    #*************************************************************************
    
    resp = {}
    
    if 'OPENSHIFT_MYSQL_DB_URL' in os.environ:
        resp['rhc-msyql'] = openshift_mysql()
    
    if 'OPENSHIFT_POSTGRESQL_DB_URL' in os.environ:
        resp['rhc-pgsql'] = openshift_pgsql()
    
    return resp

