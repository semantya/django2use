#-*- coding: utf-8 -*-

import operator, inspect

def singleton(hnd):
    return hnd()

def inherits(target, *profiles, **opts):
    opts['implicit']  = opts.get('implicit', True)
    opts['recursive'] = opts.get('recursive', True)
    
    op = operator.and_
    
    if opts['implicit']:
        op = operator.or_
    
    cb = lambda target: [target]
    
    if opts['recursive']:
        cb = lambda target: [target]+list(getattr(target, '__bases__', []))
    
    if inspect.isclass(target):
        return reduce(op, [issubclass(t, p) for t in cb(target) for p in profiles] or [not opts['implicit']])
    else:
        return False

