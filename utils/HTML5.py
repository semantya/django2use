#-*- coding: utf-8 -*-

#####################################################################################

class DynamicNode(object):
    def __init__(self, widget):
        self._attrs = {}
        
        if hasattr(self, 'initialize'):
            if callable(self.initialize):
                self.initialize(*args, **kwargs)
    
    attributes = property(lambda self: self._attrs)
    
    def __call__(self, narrow, *args, **kwargs):
        if 'class' in kwargs:
            lst = kwargs['class'] ; del kwargs['class']
            
            if type(lst) not in (set,list,frozenset):
                lst = [lst]
            
            lst = ' '.join(lst)
        
        self.invoke(*args, **kwargs)
        
        for target in self.narrow:
            self._attrs[target] = narrow
        
        return '<%(tag)s %(attrs)s></%(tag)s>' % dict(
            tag   = self.tag,
            attrs = [
                '%s="%s"' % (key, self._attrs[key])
                for key in self._attrs
            ],
        )

#####################################################################################

class Div(DynamicNode):
    def initialize(self):
        self.tag    = 'div'
        self.narrow = ['id']
    
    def invoke(self, context, value, default=None):
        pass

#************************************************************************************

class Span(DynamicNode):
    def initialize(self):
        self.tag    = 'span'
        self.narrow = ['id']
    
    def invoke(self, context, value, default=None):
        pass

#************************************************************************************

class Image(DynamicNode):
    def initialize(self):
        self.tag    = 'img'
        self.narrow = ['id']
    
    def invoke(self, context, src, width='auto', height='auto'):
        pass

#************************************************************************************

class Anchor(DynamicNode):
    def initialize(self):
        self.tag    = 'a'
        self.narrow = ['id']
    
    def invoke(self, context, value):
        pass

#************************************************************************************

class Input(DynamicNode):
    def initialize(self):
        self.tag    = 'input'
        self.narrow = ['id', 'name']
    
    def invoke(self, context, value):
        self.content        = None
        
        self.attrs['value'] = value

#************************************************************************************

class TextArea(DynamicNode):
    def initialize(self):
        self.tag    = 'textarea'
        self.narrow = ['id', 'name']
    
    def invoke(self, context, content, rows=40, cols=15):
        self.content       = content
        
        self.attrs['cols'] = cols
        self.attrs['rows'] = rows

