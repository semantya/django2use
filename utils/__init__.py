#-*- coding: utf-8 -*-

from django.template import Library

class TemplateRegistry(Library):
    def regular_tag(self, hnd):
        self.filter(hnd)
        
        hnd.is_safe = True
        
        return hnd
    
    def html5_tag(handler, *args, **kwargs):
        obj = handler(*args, **kwargs)
        
        return self.regular_tag(obj)

from . import HTML5
from . import POO

