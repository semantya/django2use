#-*- coding: utf-8 -*-

from datetime import date, time, datetime
import dateutil.parser

from django import template

from django.utils import html
from django.utils import simplejson as json
from django.utils import timezone as TZ

from django.utils.numberformat import format as number_format

register = template.Library()

#####################################################################################

@register.filter(is_safe=True)
def floatdot(value, decimal_pos=2):
    return number_format(value or 0, ".", decimal_pos)

@register.filter(is_safe=True)
def money(value):
    return number_format(value or 0, ".", 2)

#************************************************************************************

def to_date(value):
    if type(value) not in (date, datetime):
        try:
            value = datetime.fromtimestamp(long(value))
        except:
            value = dateutil.parser.parse(value)
    
    return value

@register.filter(is_safe=True)
def since(value):
    return humanize_date_difference(to_date(value), datetime.now())

def humanize_date_difference(now, otherdate=None, offset=None):
    if otherdate:
        dt = otherdate - now
        offset = dt.seconds + (dt.days * 60*60*24)
    if offset:
        delta_s = offset % 60
        offset /= 60
        delta_m = offset % 60
        offset /= 60
        delta_h = offset % 24
        offset /= 24
        delta_d = offset
    else:
        raise ValueError("Must supply otherdate or offset (from now)")

    if delta_d > 1:
        if delta_d > 6:
            date = now + datetime.timedelta(days=-delta_d, hours=-delta_h, minutes=-delta_m)
            return date.strftime('%A, %Y %B %m, %H:%I')
        else:
            wday = now + datetime.timedelta(days=-delta_d)
            return wday.strftime('%A')
    if delta_d == 1:
        return "Yesterday"
    if delta_h > 0:
        return "%dh%dm ago" % (delta_h, delta_m)
    if delta_m > 0:
        return "%dm%ds ago" % (delta_m, delta_s)
    else:
        return "%ds ago" % delta_s

#************************************************************************************

@register.filter(is_safe=True)
def ucfirst(value):
    resp = unicode(value).capitalize()
    
    return resp

#####################################################################################

@register.filter(is_safe=True)
def field_css(value, arg):
    return value.as_widget(attrs={'class': arg})

#************************************************************************************


#####################################################################################

@register.filter(name='json', is_safe=True)
def to_json(value, indent=4):
    return json.dumps(value, sort_keys=True, indent=indent, separators=(',%s\n' % (' ' * indent), ':'))

#************************************************************************************

@register.filter(is_safe=True)
def pretty_json(value):
    resp = json.dumps(value, sort_keys=True, indent=4, separators=(',\n', ':'))
    
    return html.escape(resp)

#####################################################################################

@register.simple_tag(takes_context=True)
def cdn(context, *path):
    return '/'.join(['//cdn.maher.media'] + list(path))

@register.simple_tag(takes_context=True)
def media(context, *path):
    return '/'.join(['//upload.maher.media'] + list(path))

@register.simple_tag(takes_context=True)
def static(context, *path):
    if 'curr_theme' in context:
        return '/'.join(['//skins.maher.media',context['curr_theme']['name']] + list(path))
    else:
        return '/'.join(['//static.maher.media'] + list(path))

#####################################################################################

@register.simple_tag(takes_context=True)
def trans(context, litteral, lang=None):
    return litteral

#####################################################################################

"""
from django2use.utils.HTML5 import *

#*****************************************************************

a          = register.regular_tag_html5(Anchor)
span       = register.regular_tag_html5(Div, 'span')

h1         = register.regular_tag_html5(Div, 'h1')
h2         = register.regular_tag_html5(Div, 'h2')
h3         = register.regular_tag_html5(Div, 'h3')
h4         = register.regular_tag_html5(Div, 'h4')
h5         = register.regular_tag_html5(Div, 'h5')
h6         = register.regular_tag_html5(Div, 'h6')

p          = register.regular_tag_html5(Div, 'p')
div        = register.regular_tag_html5(Div)

input      = register.regular_tag_html5(Input)
file       = register.regular_tag_html5(FileUpload)
textarea   = register.regular_tag_html5(TextArea)
select     = register.regular_tag_html5(ComboSelect)

img        = register.regular_tag_html5(Image)
audio      = register.regular_tag_html5(Audio)
video      = register.regular_tag_html5(Video)

canvas     = register.regular_tag_html5(Canvas)

#*****************************************************************

label      = register.regular_tag_html5(Bootstrap.NavBar)

navbar     = register.regular_tag_html5(Bootstrap.NavBar)
section    = register.regular_tag_html5(Bootstrap.Section)
"""

