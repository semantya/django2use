#-*- coding: utf-8 -*-

from django2use.shortcuts import *

from mist import settings

class Command(Commandline):
    help = 'Control django2use functions.'
    
    def handle(self, cmd, *args, **options):
        hnd = getattr(self, 'do_%s' % cmd, None)
        
        if callable(hnd):
            hnd(*args, **options)
    
    def do_run(self, profile, *args, **kwargs):
        os.environ['PORT'] = '8000'
        
        self.shell('foreman', 'run', profile)
    
    def do_crawl(self, *spiders, **kwargs):
        for spider in spiders:
            self.shell('scrapy', 'crawl', spider, '-L', 'INFO')
    
    def do_mongo(self, profile, *args, **kwargs):
        if profile in settings.MONGO_DBs:
            cfg = settings.MONGO_DBs[profile]
            
            self.shell('mongo', cfg['database'], '--host', cfg['host'], '--port', str(cfg['port']), '-u', cfg['username'], '-p', cfg['password'])

