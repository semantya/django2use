#-*- coding: utf-8 -*-

from django2use.shortcuts import *

from mist.shop.models import *

class Command(Commandline):
    help = 'Prepare package for deploy.'
    
    def handle(self, *args, **options):
        self.shell('venv/bin/python', 'manage.py', 'runserver_plus')

