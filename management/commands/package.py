#-*- coding: utf-8 -*-

from django2use.shortcuts import *

import codecs

class Command(Commandline):
    help = 'Prepare package for deploy.'
    
    def handle(self, *args, **options):
        from mist import settings
        
        self.log('Exporting data as fixtures ...')
        
        lst = [
            y
            for x in (
                'auth.permission', 'contenttypes.contenttype', 'admin.logentry',  'sessions.session',
                'django_evolution.evolution', 'django_evolution.version',
            )+tuple([x for x in settings.EXPORT_FIXTURE_IGNORE])
            for y in ['-e', x]
        ] + [
            '>', 'files/fixtures/raw_data.json',
        ]
        
        self.shell('venv/bin/python', 'manage.py', 'dumpdata', '--format=json', *lst)
        
        #*******************************************************************************************
        
        self.log('Fixing the fixtures ...')
        
        encoded = codecs.open('files/fixtures/raw_data.json', 'r', 'utf-8').read().encode('ascii', 'backslashreplace')
        
        open('files/fixtures/raw_data.json', 'w').write(encoded)
        
        self.shell('cat', 'files/fixtures/raw_data.json', '|', 'json_xs', '>', 'files/fixtures/structured_data.json')
        
        self.shell('cp', '-f', *['files/fixtures/%s_data.json' %x for x in ('structured', 'initial')])

