#-*- coding: utf-8 -*-

from django2use.shortcuts import *

from mist.shop.models import *

class Command(Commandline):
    help = 'Prepare package for deploy.'
    
    def handle(self, *args, **options):
        self.log('-> Synchronizing database ...')
        
        self.shell('venv/bin/python', 'manage.py', 'evolve', '--hint', '--execute', '--noinput')
        
        #*******************************************************************************************
        
        self.log('-> Loading default data ...')
        
        self.shell('venv/bin/python', 'manage.py', 'populate')
        
        #*******************************************************************************************
        
        self.log('-> Collecting static files ...')
        
        self.shell('venv/bin/python', 'manage.py', 'collectstatic', '-l', '--noinput')
        
        #*******************************************************************************************
        
        self.log('-> Setting Unix permissions ...')
        
        pth = [os.path.join('files', x) for x in ('media', 'static')]
        
        #self.shell('chown', 'www-data:www-data', '-R', *pth)
        
        self.shell('chmod', '775', '-R', *pth)

