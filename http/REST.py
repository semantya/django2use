#-*- coding: utf-8 -*-

from django2use.shortcuts import *

##############################################################################################

class BaseHandler(object):
    def __init__(self, vhost, pattern, config):
        self._vh  = vhost
        self._ptn = vhost
        self._cfg = config
    
    vhost   = property(lambda self: self._vh)
    pattern = property(lambda self: self._ptn)
    config  = property(lambda self: self._cfg)
    
    def __call__(self, request, *args, **kwargs):
        setattr(request, 'template', getattr(self, 'TEMPLATE_PATH', 'layout.html'))
        setattr(request, 'context',  {})
        
        resp = None
        
        self.invoke('prepare', request, *args, **kwargs)
        
        self.invoke('initialize', request, *args, **kwargs)
        
        if self.authorize(request):
            hnd = getattr(self, request.method, getattr(self, 'generic', None))
            
            if (hnd is not None) and callable(hnd):
                print "REST> [%s] %s : %s" % (request.method, request.get_full_path(), hnd)
                
                resp = hnd(request, *args, **kwargs)
            else:
                if (request.result or request.context.get('results', None)) is not None:
                    return None
                else:
                    return request.render_tpl('PR/not-implemented.html', renderer=rnd, **request.context)
        else:
            return self.access_error(request, *args, **kwargs)
    
    def invoke(self, view, request, *args, **kwargs):
        hnd = getattr(self, view, None)
        
        if (hnd is not None) and callable(hnd):
            print "REST> (%s) %s : %s" % (view, request.get_full_path(), hnd)
            
            return hnd(request, *args, **kwargs)
        else:
            return None

##############################################################################################

class GenericHandler(BaseHandler):
    def authorize(self, request, *args, **kwargs):
        return True
    
    def access_error(self, request, *args, **kwargs):
        return None

##############################################################################################

class FormHandler(BaseHandler):
    def prepare(self, request, *args, **kwargs):
        request.context.update(dict(
            frm = self.FORM_CLASS(),
        ))
    
    def GET(self, request, *args, **kwargs):
        return None
    
    def POST(self, request, *args, **kwargs):
        request.context['frm'] = self.FORM_CLASS(request.POST)
        
        resp = None
        
        if request.context['frm'].is_valid():
            resp = self.process(request, request.context['frm'])
        else:
            resp = self.user_input(request, request.context['frm'])
        
        return resp
    
    #*****************************************************************************************
    
    def authorize(self, request, *args, **kwargs):
        return True
    
    def access_error(self, request, *args, **kwargs):
        return None
    
    def user_input(self, request, frm, *args, **kwargs):
        pass

##############################################################################################

class ModelHandler(BaseHandler):
    def prepare(self, request, *args, **kwargs):
        request.context.update(dict(
            frm = self.FORM_CLASS(),
        ))
        
        if hasattr(self, 'initialize'):
            if callable(self.initialize):
                self.initialize(request, *args, **kwargs)

