#-*- coding: utf-8 -*-

from django2use.constants import *
from django2use.shortcuts import *
from django2use.utils import *

from . import REST

##############################################################################################

class SecurityCheck(object):
    def __init__(self, x, y, op):
        self._x  = x
        self._y  = y
        
        self._op = op
    
    x  = property(lambda self: self._x)
    y  = property(lambda self: self._y)
    op = property(lambda self: self._op)
    
    def allow(self, context):
        return reduce(operator.or_, [
            self.x.allow(context),
            self.y.allow(context),
        ])
    
    def authorize(self, context, *keys):
        return reduce(operator.or_, [
            self.x.authorize(context, *atomes),
            self.y.authorize(context, *atomes),
        ])
    
    def permit(self, context, *keys):
        return reduce(operator.or_, [
            self.x.permit(context, obj, *actions),
            self.y.permit(context, obj, *actions),
        ])
    
    def error(self, context):
        for k in ('x','y'):
            arg = getattr(self, k, None)
            
            hnd = getattr(arg, 'error', None)
            
            if callable(hnd):
                hnd(context)
    
    def __and__(self, v):  return SecurityCheck(self, operator.and_, v)
    def __or__(self, v):  return SecurityCheck(self, operator.or_,  v)
    def __xor__(self, v): return SecurityCheck(self, operator.xor_,  v)

#*********************************************************************************************

class Scope(object):
    def __init__(self, keys, default=False, *args, **kwargs):
        self._keys       = keys
        self._default    = default
        
        if hasattr(self, 'initialize'):
            self.initialize(*args, **kwargs)
    
    aliases    = property(lambda self: self._keys)
    by_default = property(lambda self: self._default)
    
    def __and__(self, v):  return SecurityCheck(self, operator.and_, v)
    def __or__(self, v):  return SecurityCheck(self, operator.or_,  v)
    def __xor__(self, v): return SecurityCheck(self, operator.xor_,  v)

#*********************************************************************************************

class InlineScope(Scope):
    def initialize(self, *args, **validators):
        self._validators = validators
    
    validators = property(lambda self: self._validators)
    
    def validate(self, profile, context, *args, **kwargs):
        if profile in self.validators:
            if callable(self.validators[profile]):
                hnd = self.validators[profile]
                
                return hnd(context, *args, **kwargs)
        
        return self.by_default
    
    def allow(self, context, *keys):
        return reduce(operator.or_, [
            (key in self.aliases)
            for key in keys
            if key and self.validate('allow', context)
        ] or [self.by_default])
    
    def authorize(self, context, *atomes):
        return reduce(operator.or_, [
            self.validate('authorize', context, atom)
            for atom in atomes
        ] or [self.by_default])
    
    def permit(self, context, obj, *actions):
        return reduce(operator.or_, [
            self.validate('permit', context, obj, acte)
            for acte in actions
        ] or [self.by_default])

##############################################################################################

class Wrapper(object):
    def __init__(self, pattern, view, *args, **params):
        self._ptn     = pattern
        self._cfg     = params
        
        self._policy  = []
        
        if hasattr(self._cfg, 'policy'):
            for scp in (self._cfg['policy'] or []):
                self._scopes.append(scp)
            
            del self._cfg['policy']
        
        if inspect.isclass(view):
            if inherits(view, REST.BaseHandler, REST.FormHandler, REST.ModelHandler):
                view = view(self, pattern, params)
            else:
                view = None
        
        if callable(view):
            if not self._cfg.get('csrf', True):
                view = csrf_exempt(view)
                
                del self._cfg['csrf']
        
        self._hnd     = view
    
    config  = property(lambda self: self._cfg)
    policy  = property(lambda self: self._policy)
    view    = property(lambda self: self._hnd)
    
    def __call__(self, request, *args, **kwargs):
        cnt = Context(self, request, *args, **kwargs)
        
        return cnt()

#*********************************************************************************************

class Context(object):
    def __init__(self, wrapper, request, *args, **kwargs):
        self._wrp     = wrapper
        
        self._req     = request
        self._args    = args
        self._kwargs  = kwargs
        
        self._rest    = None
        self._resp    = None
        
        self._thm     = request.vhost.theme.get('name', 'tumbas')
        self._tpl     = None
        
        for k,v in dict(template=None, context=None, parking={}).iteritems():
            if not hasattr(self.request, k):
                setattr(self.request, k, v)
        
        self.request.context = self.request.context or {}
    
    #************************************************************************************************
    
    wrapper  = property(lambda self: self._wrp)
    
    config   = property(lambda self: self.wrapper.config)
    scope    = property(lambda self: self.wrapper.policy)
    view     = property(lambda self: self.wrapper.view)
    
    #************************************************************************************************
    
    request  = property(lambda self: self._req)
    method   = property(lambda self: self.request.method)
    def get_full_path(self): return self.request.get_full_path()
    
    args     = property(lambda self: self._args)
    kwargs   = property(lambda self: self._kwargs)
    
    environ  = property(lambda self: self.request.environ)
    REQUEST  = property(lambda self: self.request.REQUEST)
    GET      = property(lambda self: self.request.GET)
    POST     = property(lambda self: self.request.POST)
    FILES    = property(lambda self: self.request.FILES)
    COOKIE   = property(lambda self: self.request.COOKIE)
    SESSION  = property(lambda self: self.request.SESSION)
    
    user     = property(lambda self: self.request.user)
    session  = property(lambda self: self.request.session)
    
    vhost    = property(lambda self: self.request.vhost)
    page     = property(lambda self: self.request.page)
    
    #************************************************************************************************
    
    results  = property(lambda self: self._rest, lambda self, value: setattr(self, '_rest', value) or value)
    response = property(lambda self: self._resp, lambda self, value: setattr(self, '_resp', value) or value)
    
    template = property(lambda self: self._tpl,  lambda self, value: setattr(self, '_tpl',  value) or value)
    theme    = property(lambda self: self._thm,  lambda self, value: setattr(self, '_thm',  value) or value)
    
    def theme_cdn(self, *args):
        return '/'.join(['http://cdn.maher-ops.pl', 'static', 'themes', self.theme]+[x for x in args])
    
    theme_rootdir = property(lambda self: self.theme_tpl(''))
    theme_layout  = property(lambda self: self.theme_tpl('layout.html'))
    
    def theme_tpl(self, *args):
        return '/'.join(['themes', self.theme]+[x for x in args])
    
    #apis     = property(lambda self: self.request.parking['apis'])
    #cache    = property(lambda self: self.request.parking['cache'])
    #service  = property(lambda self: self.request.parking['service'])
    #stats    = property(lambda self: self.request.parking['stats'])
    #mapping  = property(lambda self: self.request.parking['mapping'])
    
    #************************************************************************************************
    
    def redirect(self, *args, **kwargs):    return self.request.redirect(*args, **kwargs)
    
    def render_json(self, *args, **kwargs): return self.request.render_json(*args, **kwargs)
    
    def resolve_tpl(self, tpl):
        resp = tpl
        
        if resp.startswith('PR://'):
            resp = resp.replace('PR://', 'skin://PR/')
        
        if resp.startswith('skin://'):
            resp = resp.replace('skin://', self.theme_tpl(''))
        
        return resp
    
    def render_tpl(self, tpl, **cnt):
        cnt['curr_request'] = self
        cnt['curr_user']    = self.request.user
        cnt['curr_layout']  = self.theme_layout
        cnt['curr_theme']   = self.vhost.theme
        
        try:
            cnt['curr_profile'] = self.request.user.get_profile()
        except:
            cnt['curr_profile'] = None
        
        return render_to_response(self.resolve_tpl(tpl), RequestContext(self.request, cnt))
    
    #************************************************************************************************
    
    def answer(self, data):
        resp = None
        
        if type(data) in STD_TYPEs:
            self.request.context['resource'] = self.resolve(data)
            
            accepts = self.request.META['HTTP_ACCEPT'].split(';')
            
            for rnd in REST_RENDERERs:
                for mmt in accepts[0].split(',') or []:
                    mmt = mmt.lower().strip()
                    
                    if mmt in REST_RENDERERs[rnd]['mimes'] and (resp is None):
                        hnd = getattr(self.wrapper.view, 'export_%s' % rnd, None)
                        
                        if (hnd is not None) and callable(hnd):
                            raw = hnd(request, resp, *args, **kwargs)
                            
                            self.response = HttpResponse(raw, mimetype=mmt)
                        elif REST_RENDERERs[rnd]['dump']:
                            self.response = HttpResponse(REST_RENDERERs[rnd]['dump'](self.request.context['resource']), mimetype=mmt)
            
            print "\t-> default rendering using : %s => %s" % (self.request.template, data)
            
            resp = self.render_tpl(self.request.template, results=data, **self.context)
        else:
            resp = self.render_tpl('PR/not-implemented.html', **self.context)
        
        return resp
    
    #************************************************************************************************
    
    def resolve(self, x):
        if type(x) in STD_SCALARs:
            return x
        elif type(x) in STD_LISTs:
            return [
                self.resolve(y)
                for y in x
            ]
        elif type(x) in STD_HASHs:
            return dict([
                (self.resolve(k), self.resolve(v))
                for k,v in x.iteritems()
            ])
        elif hasattr(x, '__rest__'):
            return x.__rest__(self)
        elif hasattr(x, '__dict__'):
            y = x.__dict__
            
            if callable(y):
                y = y()
            
            return y
    
    #************************************************************************************************
    
    def __call__(self):
        ag = self.args
        kw = self.kwargs
        
        resp = None
        
        import mist.api.policy
        
        print self.vhost, self.vhost.policy
        
        if self.vhost.policy.allow(self):
            if self.response is None:
                resp = self.wrapper.view(self, *ag, **kw)
        else:
            if hasattr(self.vhost.policy, 'error'):
                if callable(self.vhost.policy.error):
                    self.vhost.policy.error(self)
            
            return self.response or self.render_tpl('PR/insufficient-creds.html')
        
        self.request.template = self.request.template or getattr(self.wrapper.view, 'TEMPLATE_PATH', None)
        
        self.response = resp or self.response
        
        if issubclass(type(self.response), HttpResponse) or issubclass(type(self.response), HttpResponseRedirect):
            return self.response
        else:
            if inherits(type(self.response), models.Model):
                self.results = self.resolve(self.response.__dict__)
        
        if (self.results is None) and ('frm' in self.context):
            self.results = {}
        
        if (self.response is None) and (self.results is not None):
            self.response = self.answer(self.results)
        
        return self.response

##############################################################################################

class mgr(object):
    def __init__(self):
        self._mapping = {}
        self._factory = {}
    
    def policy(self, *args, **kwargs):
        def do_apply(hnd, nrw, *a, **kw):
            resp = hnd
            
            if type(nrw) in STD_LISTs:
                nrw = [x for x in nrw]
            else:
                nrw = [nrw]
            
            if inherits(hnd, Scope, recursive=True):
                resp = hnd(nrw, *a, **kw)
                
                self._factory[resp] = []
                
                for key in resp.aliases:
                    self._mapping[key] = resp
            
            return resp
        
        return lambda hnd: do_apply(hnd, *args, **kwargs)
    
    def find(self, *keys):
        resp = []
        
        for key in keys:
            if key in self._mapping:
                resp.append(self._mapping[key])
        
        if len(keys)==1:
            if len(resp)==1:
                return resp[0]
            else:
                return None
        else:
            return resp
    
    def resolve(self, *narrows):
        resp = []
        
        for nrw in narrows:
            if not self.is_scope(nrw):
                resp.append(self.find(nrw))
            
            resp.append(nrw)
        
        return resp
    
    def is_scope(self, obj):
        return inherits(type(obj), Scope, recursive=True)
    
    def __getitem__(self, key): return self.resolve(key)
    def __iter__(self):         return self._factory.keys().__iter__()

mgr = mgr()

def policy(*args, **kwargs):
    return mgr.policy(*args, **kwargs)

#################################################################################################

@mgr.policy('maher.access.dummy')
class DummyAccess(Scope):
    def allow(self, request, *args, **kwargs):
        return True

