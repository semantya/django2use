#-*- coding: utf-8 -*-

from django2use.shortcuts import *

from django.conf import settings

from . import NSAP, REST

##############################################################################################

class vHost(object):
    DEFAULT_POLICY = NSAP.DummyAccess
    
    def __init__(self, urlconf, theme, **kwargs):
        self._uc  = urlconf
        self._thm = theme
        
        self._fq  = kwargs.get('fqdns', [])
        self._plc = kwargs.get('policy', None)
        
        self._rt  = []
        self._mn  = []
        
        for key in ('fqdns','policy'):
            if key in kwargs:
                del kwargs[key]
        
        self._cfg = kwargs
    
    urlconf   = property(lambda self: self._uc)
    theme     = property(lambda self: self._thm)
    
    routes    = property(lambda self: self._rt)
    policy    = property(lambda self: self._plc or self.DEFAULT_POLICY)
    
    menus     = property(lambda self: self._mn)
    config    = property(lambda self: self._cfg)
    
    domains   = property(lambda self: self._fq)
    fqdn      = property(lambda self: self.domains[0])
    url       = property(lambda self: 'http://%s/' % self.fqdn)
    
    patterns  = property(lambda self: dj_urls.patterns(self.urlconf, *self._rt))
    
    def register(self, *args, **kwargs):
        def do_apply(hnd, ptn, *a, **kw):
            return self.route(ptn, hnd)
        
        return lambda hnd: do_apply(hnd, *args, **kwargs)
    
    def route(self, ptn, hnd, *args, **kwargs):
        resp = hnd
        
        wrap = kwargs.get('wrap', True)
        
        if 'wrap' in kwargs:
            del kwargs['wrap']
        
        if wrap:
            resp = NSAP.Wrapper(ptn, hnd, *args, **kwargs)
            
            self._rt += [dj_urls.url(ptn, resp)]
        else:
            self._rt += [dj_urls.url(ptn, hnd, *args, **kwargs)]
        
        return resp
    
    def add_menu(self, **kwargs):
        self._mn += [dict(**kwargs)]
        
        return lambda hnd: hnd
    
    def __str__(self): return self.fqdn

##############################################################################################

def declare(key, *args, **config):
    return vHost(*args, **config)

