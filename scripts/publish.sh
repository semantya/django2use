#!/bin/bash

#*******************************************************************************************

echo "Synchronizing database schemes ..."
python manage.py syncdb --noinput

#*******************************************************************************************

echo "-> Evolving database ..."

python manage.py evolve --hint --execute --noinput

#*******************************************************************************************

echo "-> Loading default data ..."

python manage.py populate

