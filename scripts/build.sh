#!/bin/bash

#*******************************************************************************************

if [[ -d $OPENSHIFT_DATA_DIR ]] ; then
    mkdir -p $OPENSHIFT_DATA_DIR/{media,static}
fi

#*******************************************************************************************

echo "-> Compiling source files ..."

python manage.py compile_pyc

#*******************************************************************************************

echo "-> Collecting static files ..."

python manage.py collectstatic -l --noinput

